import 'package:admin_app/quest_form.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class QuestionInformation extends StatefulWidget {
  QuestionInformation({Key? key}) : super(key: key);

  @override
  _QuestionInformationState createState() => _QuestionInformationState();
}

class _QuestionInformationState extends State<QuestionInformation> {
  final Stream<QuerySnapshot> _QuestionStream = FirebaseFirestore.instance
      .collection("question")
      .orderBy('id', descending: false)
      .snapshots();

  CollectionReference Questions =
      FirebaseFirestore.instance.collection("question");
  Future<void> delQuestion(questionId) {
    return Questions.doc(questionId)
        .delete()
        .then((value) => print('Question Delete'))
        .catchError((error) => print("Failed to Delete Question $error"));
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder(
      stream: _QuestionStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text("Something went Wrong!!");
        } else if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading..");
        }
        return ListView(
          children: snapshot.data!.docs.map((DocumentSnapshot document) {
            Map<String, dynamic> data =
                document.data()! as Map<String, dynamic>;
            return ListTile(
              title: Text(data['questions']),
              subtitle: Text(data['answer']),
              onTap: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) =>
                            QuestionForm(questionId: document.id)));
              },
              trailing: IconButton(
                icon: Icon(Icons.delete),
                onPressed: () async {
                  await delQuestion(document.id);
                },
              ),
            );
          }).toList(),
        );
      },
    );
  }
}
