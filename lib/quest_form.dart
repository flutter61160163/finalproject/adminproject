import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class QuestionForm extends StatefulWidget {
  String questionId;
  QuestionForm({Key? key, required this.questionId}) : super(key: key);

  @override
  _QuestionFormState createState() => _QuestionFormState(this.questionId);
}

class _QuestionFormState extends State<QuestionForm> {
  String questionId;
  String newId = "";
  String question = "";
  String answer = "";
  String fake = "0";
  _QuestionFormState(this.questionId);
  TextEditingController _idControler = new TextEditingController();
  TextEditingController _questionControler = new TextEditingController();
  TextEditingController _answerControler = new TextEditingController();
  TextEditingController _fakeControler = new TextEditingController();
  CollectionReference questions =
      FirebaseFirestore.instance.collection('question');

  Future<void> addQuestion() {
    return questions
        .add({
          'id': this.newId,
          'questions': this.question,
          'answer': this.answer,
          'fake': this.fake,
        })
        .then((value) => print("Question Added"))
        .catchError((error) => print("Failed to add question: $error"));
  }

  Future<void> updateQuestion() {
    return questions
        .doc(this.questionId)
        .update({
          'id': this.newId,
          'questions': this.question,
          'answer': this.answer,
          'fake': this.fake
        })
        .then((value) => print("Question Updated"))
        .catchError((error) => print("Failed to update question: $error"));
  }

  @override
  void initState() {
    super.initState();
    if (this.questionId.isNotEmpty) {
      questions.doc(this.questionId).get().then((snapshot) {
        if (snapshot.exists) {
          var data = snapshot.data() as Map<String, dynamic>;
          newId = data["id"];
          question = data["questions"];
          answer = data["answer"];
          fake = data["fake"];
          _idControler.text = newId;
          _questionControler.text = question;
          _answerControler.text = answer;
          _fakeControler.text = fake;
        }
      });
    }
  }

  final _formKey = GlobalKey<FormState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Question"),
      ),
      body: Form(
        autovalidateMode: AutovalidateMode.onUserInteraction,
        key: _formKey,
        child: Column(
          children: [
            TextFormField(
              controller: _idControler,
              decoration: InputDecoration(labelText: 'ข้อที่'),
              onChanged: (value) {
                setState(() {
                  newId = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "กรุณาเขียนลำดับข้อ";
                }
                return null;
              },
            ),
            TextFormField(
              controller: _questionControler,
              decoration: InputDecoration(labelText: 'คำถาม'),
              onChanged: (value) {
                setState(() {
                  question = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "กรุณาเขียนคำถาม";
                }
                return null;
              },
            ),
            TextFormField(
              controller: _answerControler,
              decoration: InputDecoration(labelText: 'คำตอบ'),
              onChanged: (value) {
                setState(() {
                  answer = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "กรุณาเขียนคำตอบ";
                }
                return null;
              },
            ),
            TextFormField(
              controller: _fakeControler,
              decoration: InputDecoration(labelText: 'คำตอบหลอก'),
              onChanged: (value) {
                setState(() {
                  fake = value;
                });
              },
              validator: (value) {
                if (value == null || value.isEmpty) {
                  return "กรุณาเขียนคำตอบหลอก";
                }
                return null;
              },
            ),
            Padding(padding: EdgeInsets.all(8)),
            ElevatedButton(
                onPressed: () async {
                  if (_formKey.currentState!.validate()) {
                    if (questionId.isNotEmpty) {
                      await updateQuestion();
                    } else {
                      await addQuestion();
                    }

                    Navigator.pop(context);
                  }
                },
                child: Text("บันทึก"))
          ],
        ),
      ),
    );
  }
}
